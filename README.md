### obfs4proxy/meek_lite utls fork
#### Yawning Angel (yawning at schwanenlied dot me)

#### Forked by DominusMars

### What?

This is a fork of a fork because I needed other functionality  


This is a fork of [utls][1] for the specific purpose of improving
obfs4proxy's meek_lite transport.

Functional differences:
 * Fixed Some TLS clients, adding Lastest Chrome. 

### Why?

Never gonna give you up, never gonna let you down. 

### Why don't you upstream the changes?

It's a pet project done in my spare time and I want to use a
[strong/viral][3] copyleft license for the vast majority of my pet
projects going forward.

I used to have a more liberal view on licensing but certain entities have
ruined it for everybody.  The portions of the code that I have not written
or altered are naturally under the original license.


[1]: https://github.com/refraction-networking/utls
[2]: https://datatracker.ietf.org/doc/draft-ietf-tls-certificate-compression/
[3]: https://www.gnu.org/licenses/gpl.txt
